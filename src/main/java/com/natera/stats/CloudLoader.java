package com.natera.stats;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.dnanexus.*;

public class CloudLoader {

    private static final String token = "JdFGTROAIR8555RLjbkzkBPdogc3To9I";

    public Map<String, String> getCloudFileProperties(final String sampleId, final String projectId) {
        final DXEnvironment dxEnvironment = DXEnvironment.Builder.fromDefaults().setBearerToken(token).build();

        final List<DXFile> dxFiles = DXSearch.findDataObjectsWithEnvironment(dxEnvironment).withClassFile()
                .withState(DataObjectState.CLOSED)
                .inProject(DXProject.getInstance(projectId)).withProperty("sequencingSampleId", sampleId)
                .includeDescribeOutput(DXDataObject.DescribeOptions.get().withProperties().withDetails())
                .execute().asList();

        final DXFile dxFile = dxFiles.get(0);

        if (dxFile != null) {
            System.out.println("File found");
            return dxFile.getCachedDescribe().getProperties();
        }

        System.out.println("File not found");
        return new HashMap<>();
    }
}
